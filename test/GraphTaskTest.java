package test;
import static org.junit.Assert.*;
import org.junit.Test;

import src.GraphTask;
import src.GraphTask.*;


/** Testklass.
 * @author laur
 */
public class GraphTaskTest {

   @Test
   public void test1() { 
	   GraphTask task = new GraphTask();
	   Graph g = task.new Graph("test1");
	   g.createCompleteGraph(6);
	   int numOfEdges = g.edges();
	   assertEquals("Expected: %d , Actual : %d"  , 15, numOfEdges);
	   
   }
   @Test
   public void test2(){
	   GraphTask task = new GraphTask();
	   Graph g = task.new Graph("test2");
	   g.createCompleteGraph(6);
	   int numOfVert = g.vertices();
	   assertEquals("Expected: %d , Actual : %d"  , 6, numOfVert);
	   
   }
   @Test (expected=IllegalArgumentException.class)
   public void test3(){
	   GraphTask task = new GraphTask();
	   Graph g = task.new Graph("test3");
	   g.createCompleteGraph(1);
	   
   }
   @Test
   public void test4(){
	   GraphTask task = new GraphTask();
	   Graph g = task.new Graph("test4");
	   g.createCompleteGraph(3);
	   assertTrue("Complete graph can't be 0 graph", g.edges() != 0);
   }
   @Test (expected=IllegalArgumentException.class)
   public void test5(){
	   GraphTask task = new GraphTask();
	   Graph g = task.new Graph("test5");
	   g.createCompleteGraph(-2);
   }
}

