package src;

public class GraphTask {

	public static void main(String[] args) {
		GraphTask a = new GraphTask();
		a.run();
	}

	public void run() {
		Graph g = new Graph("G");
		g.createCompleteGraph(5);
		System.out.println(g);

	}

	/**
	 * Represents a vertex in a graph.
	 */
	class Vertex {

		private String id;
		private Vertex next;
		private Arc first;
		private int info = 0;

		Vertex(String s, Vertex v, Arc e) {
			id = s;
			next = v;
			first = e;
		}

		Vertex(String s) {
			this(s, null, null);
		}

		@Override
		public String toString() {
			return id;
		}

	}

	/**
	 * Represents an arc in a graph.
	 */
	class Arc {

		private String id;
		private Vertex target;
		private Arc next;

		Arc(String s, Vertex v, Arc a) {
			id = s;
			target = v;
			next = a;
		}

		Arc(String s) {
			this(s, null, null);
		}

		@Override
		public String toString() {
			return id;
		}

	}

	/**
	 * Represents a graph
	 */
	public class Graph {

		private String id;
		private Vertex first;
		private int info = 0;

		Graph(String s, Vertex v) {
			id = s;
			first = v;
		}

		public Graph(String s) {
			this(s, null);
		}

		/**
		 * This method will help print out our graph in adjacency list structure
		 * 
		 * @return
		 */
		@Override
		public String toString() {
			String nl = System.getProperty("line.separator");
			StringBuffer sb = new StringBuffer(nl);
			sb.append("Graph: " + id);
			sb.append(nl);
			Vertex v = first;
			while (v != null) {
				sb.append(v.toString());
				sb.append(" -->");
				Arc a = v.first;
				while (a != null) {
					sb.append(" ");
					sb.append(a.toString());
					sb.append(" (");
					sb.append(v.toString());
					sb.append("->");
					sb.append(a.target.toString());
					sb.append(")");
					a = a.next;
				}
				sb.append(nl);
				v = v.next;
			}
			return sb.toString();
		}

		/**
		 * Creates new Vertex
		 * 
		 * @param vid
		 * @return Vertex
		 */
		public Vertex createVertex(String vid) {
			Vertex res = new Vertex(vid);
			res.next = first;
			first = res;
			return res;
		}

		/**
		 * Creates new Arc
		 * 
		 * @param aid
		 * @param from
		 * @param to
		 * @return Arc
		 */
		public Arc createArc(String aid, Vertex from, Vertex to) {
			Arc res = new Arc(aid);
			res.next = from.first;
			from.first = res;
			res.target = to;
			return res;
		}

		/**
		 * Create a connected undirected random tree with n vertices. Each new
		 * vertex is connected to some random existing vertex.
		 * 
		 * @param n number of vertices added to this graph
		 */
		public void createRandomTree(int n) {
			if (n <= 0)
				return;
			if (n > 100) {
				throw new IllegalArgumentException("Too many vertices! " + n);
			}
			Vertex[] varray = new Vertex[n];
			for (int i = 0; i < n; i++) {
				varray[i] = createVertex("v" + String.valueOf(n - i));
				if (i > 0) {
					int vnr = (int) (Math.random() * i);
					createArc("a" + varray[vnr].toString() + "_" + varray[i].toString(), varray[vnr], varray[i]);
					createArc("a" + varray[i].toString() + "_" + varray[vnr].toString(), varray[i], varray[vnr]);
				} else {
				}
			}
		}

		/**
		 * Create an adjacency matrix of this graph. Side effect: corrupts info
		 * fields in the graph
		 * 
		 * @return adjacency matrix
		 */
		public int[][] createAdjMatrix() {
			info = 0;
			Vertex v = first;
			while (v != null) {
				v.info = info++;
				v = v.next;
			}
			int[][] res = new int[info][info]; // create n x n 2D array
			v = first;
			while (v != null) {
				int i = v.info;
				Arc a = v.first;
				while (a != null) {
					int j = a.target.info;
					res[i][j]++;
					a = a.next;
				}
				v = v.next;
			}
			return res;
		}

		/**
		 * Create a connected complete graph (undirected, no loops, no multiple
		 * arcs) with n vertices where n > 1
		 * 
		 * @param n
		 *            number of vertices
		 */
		public void createCompleteGraph(int n) {
			if (n < 0)
				throw new IllegalArgumentException("Graph cannot have negative number of vertices: " + n);
			if (n >= 0 && n < 2) {
				throw new IllegalArgumentException("Complete graph needs at least 2 vertices. You gave " + n);
			}
			int m = (n * (n - 1)) / 2; // number of complete graph edges
			first = null;
			createRandomTree(n); // n-1 edges created here
			Vertex[] vert = new Vertex[n];
			Vertex v = first;
			int c = 0;
			while (v != null) {
				vert[c++] = v;
				v = v.next;
			}
			// Adjacency matrix structure helps us determine adjacencies
			// between pairs
			int[][] connected = createAdjMatrix();
			int edgeCount = m - n + 1; // remaining edges
			// create remaining edges where vertices are not adjacent
			while (edgeCount > 0) {
				int i = (int) (Math.random() * n); // random source
				int j = (int) (Math.random() * n); // random target
				if (i == j)
					continue; // no loops
				if (connected[i][j] != 0 || connected[j][i] != 0)
					continue; // no multiple edges
				Vertex vi = vert[i];
				Vertex vj = vert[j];
				createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
				connected[i][j] = 1;
				createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
				connected[j][i] = 1;
				edgeCount--; // a new edge happily created
			}

		}

		/**
		 * This method is meant for testing purposes. Counts the graph edges
		 * 
		 * @return count
		 */
		public int edges() {
			int count = 0;
			Vertex v = first;
			while (v != null) {
				Arc a = v.first;
				while (a != null) {
					count++; // if arc exists, count it
					a = a.next;
				}
				v = v.next; // move to next vertex (if there is one)
			}
			return count / 2;
		}

		/**
		 * This method is meant for testing purposes. Counts graph vertices
		 * 
		 * @return count
		 */
		public int vertices() {
			int count = 0;
			Vertex v = first;
			while (v != null) {
				count++;
				Arc a = v.first;
				while (a != null) {
					a = a.next;
				}
				v = v.next; // move to next vertex (if there is one)
			}
			return count;
		}
	}

}

